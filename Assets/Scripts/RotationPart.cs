using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationPart : MonoBehaviour
{
    public bool IsDone = false;

    public void Apply()
    {
        IsDone = true;
    }
}
