using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class AsteroidLogic : MonoBehaviour
{
    public float speed;

    private DanjetrZone dzone;
    public bool isyes = false;

    private Asteroid asteroid;

    void Start()
    {
        GetComponent<Renderer>().material.SetFloat("_Scale", Random.Range(-1.0f, 1.0f));

        transform.localScale = new Vector3(Random.Range(1.0f, 2.0f), Random.Range(1.0f, 2.0f), Random.Range(1.0f, 2.0f));
        //transform.localRotation = Quaternion.Euler(Random.Range(0.0f, 180.0f), Random.Range(0.0f, 180.0f), Random.Range(0.0f, 180.0f));

        dzone = GameObject.FindGameObjectWithTag("danger").GetComponent<DanjetrZone>();
        asteroid = GameObject.Find("[Asteroid_controller]").GetComponent<Asteroid>();
    }

    void Update()
    {
        if (!asteroid.AtackFor2range)
        {
            if (gameObject.transform.position.x < -60f)
            {
                Destroy(gameObject);
            }
            gameObject.GetComponent<Rigidbody>().AddForce(-transform.right * speed, ForceMode.Impulse);
        }
        else
        {
            if (gameObject.transform.position.x > 60f)
            {
                Destroy(gameObject);
            }
            gameObject.GetComponent<Rigidbody>().AddForce(transform.right * speed, ForceMode.Impulse);
        }
      
    }

    void OnTriggerEnter(Collider other)
    {
        isyes = true;
        if (other.tag=="Cheat")
        {
            //if (kolvokorb==3)
            //{
            //   SceneManager.LoadScene("MainMenu");
            //}
            dzone.kolvo--;
            Destroy(gameObject);
        }
    }
}
