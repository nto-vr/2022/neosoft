using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Valve.VR;
using Valve.VR.InteractionSystem;

public class Turret : MonoBehaviour
{
    public Image ChargeImage;
    public Image ViewImage;
    public Image ShotImage;
    public Text ReturnImage;

    public GameObject PlayerViewPosition;
    public GameObject PlayerReturnPosition;
    public GameObject CatchedObject = null;

    public GameObject BulletPrefab;

    public GameObject ExplosionPrefab;

    public GameObject BulletSpawn;

    public SteamVR_Action_Boolean action;
    public SteamVR_Input_Sources sources;

    public Teleport teleport;
    public PlayerMove player;

    public LineRenderer r;

    private DanjetrZone dzone;

    int state = 0;
    GameObject bullet;

    // Start is called before the first frame update
    void Start()
    {
        player = Player.instance.gameObject.GetComponent<PlayerMove>();
        dzone = GameObject.FindGameObjectWithTag("danger").GetComponent<DanjetrZone>();
    }

    // Update is called once per frame
    void Update()
    {
        if (state == 2)
        {
            if (action.GetStateDown(sources))
            {
                GoBack();
            }
        }
            
       
        if (state == 2)
        {
            r.SetPosition(0, Player.instance.hmdTransform.position);
            r.SetPosition(1, Player.instance.hmdTransform.TransformDirection(Vector3.forward) * 5000);
            int layerMask = 1 << 6;


            RaycastHit hit;
            if (Physics.Raycast(Player.instance.hmdTransform.position, Player.instance.hmdTransform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity, layerMask))
            {
                CatchedObject = hit.collider.gameObject;
                ReturnImage.color = Color.green;
                Debug.DrawRay(Player.instance.hmdTransform.position, Player.instance.hmdTransform.TransformDirection(Vector3.forward) * 5000, Color.white);
            }
            else
            {
                CatchedObject = null;
                ReturnImage.color = Color.red;
                Debug.DrawRay(Player.instance.hmdTransform.position, Player.instance.hmdTransform.TransformDirection(Vector3.forward) * 5000, Color.white);
            }

            
        }
        if (state == 3)
        {
            if (CatchedObject == null)
            {
                return;
            }

            r.SetPosition(0, transform.position);
            r.SetPosition(1, CatchedObject.transform.position);
        }
    }

    public void Charge()
    {
        if (state == 0)
        {
            ChargeImage.color = Color.green;
            ViewImage.color = Color.yellow;
            state = 1;
        }
    }

    public void View()
    {
        if (state == 1)
        {
            Player.instance.transform.position = PlayerViewPosition.transform.position;
            teleport.enabled = false;
            player.enabled = false;
            ViewImage.color = Color.green;
            ShotImage.color = Color.yellow;

            state = 2;
        }
    }

    public void Shot()
    {
        if (state == 3)
        {
            if (bullet != null)
            {
                return;
            }

            bullet = Instantiate (BulletPrefab);
            bullet.transform.position = BulletSpawn.transform.position;

            StartCoroutine(Bullet());
        }
    }

    IEnumerator Bullet()
    {
        while (Vector3.Distance(bullet.transform.position, CatchedObject.transform.position) > 1.0f)
        {
            bullet.transform.position += (CatchedObject.transform.position - bullet.transform.position).normalized * 0.1f;
            yield return new WaitForFixedUpdate();
        }

        r.SetPosition(0, Vector3.zero);
        r.SetPosition(1, Vector3.zero);

        state = 0;

        var explosion = Instantiate(ExplosionPrefab);
        explosion.transform.position = CatchedObject.transform.position;

        Destroy(bullet);
        Destroy(CatchedObject);

        if (CatchedObject.GetComponent<AsteroidLogic>().isyes)
        {
            dzone.kolvo--;
        }

        GameObject UIas = GameObject.FindGameObjectWithTag("UIAstro");
        UIas.GetComponent<UIAsteroid>().kolvo++;

        GameObject Txt = GameObject.FindGameObjectWithTag("Cheat");
        Txt.GetComponent<CheatController>().o_kovlo++;






        bullet = null;
        CatchedObject = null;

        ViewImage.color = Color.red;
        ShotImage.color = Color.red;
        ChargeImage.color = Color.yellow;

        
    }

    public void GoBack()
    {
        if (CatchedObject != null)
        {
            state = 3;
            
        } else
        {
            ChargeImage.color = Color.green;
            ViewImage.color = Color.yellow;
            ShotImage.color = Color.red;
            state = 1;
        }
        Player.instance.transform.position = PlayerReturnPosition.transform.position;
        teleport.enabled = true;
        player.enabled = true;
    }
}
