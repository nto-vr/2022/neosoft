using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;


public class Door : MonoBehaviour
{
    public Animator door1to;
    bool isopened = false;
    public string tag = "ChechDoor1";

    void Update()
    {
        if (Vector3.Distance(Player.instance.transform.position, GameObject.FindGameObjectWithTag(tag).transform.position) <2f)
        {
            if (!isopened)
            {
                door1to.Play("DoorOpen", 0, 0.0f);
                isopened = true;
            }
           
        }
        else
        {
            if (isopened)
            {
                door1to.Play("DoorClose", 0, 0.0f);
                isopened = false;
            }
        }
    }
}
