using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class PlayerStartPosition : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Player.instance.transform.position = transform.position;
    }

}
