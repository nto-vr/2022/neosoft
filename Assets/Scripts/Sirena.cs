using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sirena : MonoBehaviour
{
    private AudioSource aud;
    private DanjetrZone dzone;
    private Light lght;

    void Start()
    {
        dzone = GameObject.FindGameObjectWithTag("danger").GetComponent<DanjetrZone>();
        lght = gameObject.GetComponent<Light>();
        aud = gameObject.GetComponent<AudioSource>();
    }


    void Update()
    {
        if (dzone.kolvo>0)
        {
            StartSirene();
        }
        else
        {
            StopSirene();
        }
    }
    public void StartSirene()
    {
        if (!aud.isPlaying)
        {
            aud.Play();
        }

        if (lght.intensity>=0 && lght.intensity<=200)
        {
            lght.intensity +=5;
            
        }
        else if (lght.intensity >= 0 && lght.intensity >= 200)
        {
            lght.intensity -= 5;
        }
    }

    public void StopSirene()
    {
        lght.intensity = 0;
        aud.Stop();
    }
}
