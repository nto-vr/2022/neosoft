using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CheatController : MonoBehaviour
{
    public GameObject[] polomki;

    public int o_kovlo;
    public int pop_kolv;

    public Text[] o_kolvo1;
    public Text[] pop_kolvo1;

    public Text[] polomkitxt;

    void Update()
    {
        for (int i=0;i<o_kolvo1.Length;i++)
        {
            o_kolvo1[i].text = "����� ����� "+o_kovlo+" ����������";
            pop_kolvo1[i].text = "� ������� ������ "+pop_kolv+" ����������";
        }

        if (pop_kolv == 1)
        {
            for (int i = 0; i < o_kolvo1.Length; i++)
            {
                polomkitxt[i].text = "��������! ���������� ������� �������";
            }

            polomki[0].SetActive(false);
            polomki[1].SetActive(true);
          

        }
        else if(pop_kolv == 2)
        {
            for (int i = 0; i < o_kolvo1.Length; i++)
            {
                polomkitxt[i].text = "��������! ���������� ������� �������!\n�����!\n������ �����!";
            }

            polomki[2].SetActive(true);

        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag=="meteor")
        {
            gameObject.GetComponent<AudioSource>().Play();
            pop_kolv++;
            if (pop_kolv==3)
            {
                SetResult("score", o_kovlo);
                SceneManager.LoadScene("MainMenu");
            }
        }
    }

    public void SetResult(string KeyName, int Value)
    {
        PlayerPrefs.SetInt(KeyName, Value);
    }
}
