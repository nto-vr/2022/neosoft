using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScenesLoader : MonoBehaviour
{
    public int currentScene;
    public int newScene;
    public GameObject loadingScreen;
    public GameObject clicker;
    public GameObject exit;
    AsyncOperation asyncLoad;
    private bool _load = false;

    public Slider bar;
    public void GameExit()
    {
        Application.Quit();
    }

    public void GoToGame()
    {
        if (!_load)
        {
            loadingScreen.SetActive(true);
            StartCoroutine(LoadAsync());
            exit.SetActive(false);
            _load = true;
        }
        else
        {
            if (asyncLoad.progress >= 0.9f && !asyncLoad.allowSceneActivation)
            {

                asyncLoad.allowSceneActivation = true;

            }
        }
        
    }

    IEnumerator LoadAsync()
    {
        asyncLoad = SceneManager.LoadSceneAsync(newScene);
        asyncLoad.allowSceneActivation = false;
        clicker.SetActive(true);
        while (true)
        {
            bar.value = asyncLoad.progress;

            yield return null;
        }
    }

}
