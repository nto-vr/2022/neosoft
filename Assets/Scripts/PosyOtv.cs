using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PosyOtv : MonoBehaviour
{
    private GameObject[] cubes;

    void Update()
    {
        string lname = SceneManager.GetActiveScene().name;
        if (lname=="Space")
        {
            cubes = GameObject.FindGameObjectsWithTag("cubes");

            for (int i=0;i<cubes.Length;i++)
            {
                if (Vector3.Distance(transform.position, cubes[i].transform.position)<0.1f)
                {
                    cubes[i].transform.GetComponent<Provider>().check = true;
                }
            }
        }
    }

}
