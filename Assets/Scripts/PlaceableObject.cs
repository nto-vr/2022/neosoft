using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
public class PlaceableObject : MonoBehaviour
{
    public Interactable MyObject;
    public bool IsDone = false;

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(MyObject.transform.position, transform.position) < 0.2f)
        {
            MyObject.transform.position = transform.position;
            MyObject.transform.rotation = transform.rotation;
            MyObject.enabled = false;
            MyObject.GetComponent<Rigidbody>().isKinematic = true;
            IsDone = true;
        }
    }
}
