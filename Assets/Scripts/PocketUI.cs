using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class PocketUI : MonoBehaviour
{
    public SteamVR_Action_Boolean action;
    public SteamVR_Input_Sources sources;

    public GameObject ui;

    private void Update()
    {
        if (action.GetStateDown(sources))
        {
            ui.SetActive(!ui.activeSelf);
        }
    }
}
