using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    public GameObject[] doors;
    public GameObject[] lamps;

    public GameObject[] providers;


    void Start()
    {
        StartCoroutine(WaitPolomka((float)Random.Range(4,15)));
    }


    void Update()
    {
        for (int i=0;i<providers.Length;i++)
        {
            if (providers[i].transform.GetComponent<Provider>().check)
            {
                if (i==0|| i == 1)
                {
                    doors[0].GetComponent<Door>().enabled = true;
                    doors[1].GetComponent<Door>().enabled = true;
                    lamps[0].GetComponent<Light>().intensity = 0;
                    lamps[1].GetComponent<Light>().intensity = 0;
                }
                else if(i == 2 || i == 3)
                {
                    doors[2].GetComponent<Door>().enabled = true;
                    doors[3].GetComponent<Door>().enabled = true;
                    lamps[2].GetComponent<Light>().intensity = 0;
                    lamps[3].GetComponent<Light>().intensity = 0;
                }
            }
        }
    }



    private IEnumerator WaitPolomka(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        int num = Random.Range(1,3);

        if (num==1)
        {
            doors[0].GetComponent<Door>().enabled = false;
            doors[1].GetComponent<Door>().enabled = false;
            lamps[0].GetComponent<Light>().intensity = 15;
            lamps[1].GetComponent<Light>().intensity = 15;
        }
        else
        {
            doors[2].GetComponent<Door>().enabled = false;
            doors[3].GetComponent<Door>().enabled = false;
            lamps[2].GetComponent<Light>().intensity = 15;
            lamps[3].GetComponent<Light>().intensity = 15;
        }

    }
}
