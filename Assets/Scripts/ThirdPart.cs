using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPart : MonoBehaviour
{
    public GameObject MyPart;

    public bool IsDone = false;

    LineRenderer _r;

    // Start is called before the first frame update
    void Start()
    {
        _r = GetComponent<LineRenderer>();
        _r.SetPosition(0, transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        _r.SetPosition(1, MyPart.transform.position);

        if (Vector3.Distance(transform.position, MyPart.transform.position) > 0.4f)
        {
            IsDone = true;
        }
    }
}
