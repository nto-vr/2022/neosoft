using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightBehaviour : MonoBehaviour
{
    IEnumerator Start()
    {
        yield return new WaitForSeconds(Random.Range(15.0f,  25.0f));

        GetComponent<Light>().enabled = false;
    }

    public void FixLight()
    {
        GetComponent<Light>().enabled = true;
    }
}
