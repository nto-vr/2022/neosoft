using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PocketUIButtons : MonoBehaviour
{
    public GameObject soundOff;
    public GameObject soundOn;

    public void Res()
    {
        SceneManager.LoadScene(1);

    }

    public void SoundOn()
    {
        soundOff.SetActive(true);
        soundOn.SetActive(false);
        AudioListener.volume = 0.0f;
    }

    public void SoundOff()
    {
        soundOff.SetActive(false);
        soundOn.SetActive(true);
        AudioListener.volume = 1.0f;
    }
}
