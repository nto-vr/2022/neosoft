using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class PocketUIButton : MonoBehaviour
{
    public SteamVR_Action_Boolean action;
    public SteamVR_Input_Sources sources;

    public GameObject ui;
    public GameObject light;
    public GameObject tool_obj;

    private void Update()
    {
        if (action.GetStateDown(sources))
        {
            ui.SetActive(!ui.activeSelf);
            if (ui.activeSelf)
            {
                light.SetActive(false);
                tool_obj.SetActive(false);
            }
        }
    }

    public void flashlight()
    {
        light.SetActive(true);
        ui.SetActive(false);
    }

    public void tool()
    {
        tool_obj.SetActive(true);
        ui.SetActive(false);
    }
}
