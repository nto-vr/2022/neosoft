using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pause: MonoBehaviour
{
    public int currentScene;
    public GameObject resume;
    public GameObject restart;
    public GameObject exit;
    public GameObject SoundOn;
    public GameObject SoundOff;
  
 

    public void GameExit()
    {
        Application.Quit();
        Debug.Log("Exit");
    }

    public void GameResume()
    {
        resume.SetActive(false);
    }
    public void GameRestart()
    {
        SceneManager.LoadSceneAsync(currentScene);
    }
    public void SoundOfff()
    {
      
            SoundOn.SetActive(false);
            SoundOff.SetActive(true);
            AudioListener.volume = 0;
    
    }
    public void SoundOnn()
    {

        SoundOn.SetActive(true);
        SoundOff.SetActive(false);
        AudioListener.volume = 0;

    }

}
