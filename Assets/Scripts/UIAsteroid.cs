using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIAsteroid : MonoBehaviour
{
    public int kolvo;
    public Text sbit;
    public Text time;

    public Text sbit2;
    public Text time2;

    public float TimeToHit;

    public GameObject[] asteroids;
    public bool isyes = false;

    public bool goto2scene = false;

    void Update()
    {
        if (!goto2scene)
        {
            if (isyes)
            {
                asteroids = GameObject.FindGameObjectsWithTag("meteor");
                float speed = asteroids[0].GetComponent<Rigidbody>().velocity.magnitude;
                TimeToHit = (Vector3.Distance(transform.position, asteroids[0].transform.position)) / speed;
            }

            sbit.text = "�� ����� " + kolvo + " " + "����������";
            time.text = "����� �� ������������ " + Mathf.Round(TimeToHit) + " �";
        }
        else
        {

            sbit.text = "�������� ����� � ��������������� �������";
            time.text = "";
            if (isyes)
            {
                asteroids = GameObject.FindGameObjectsWithTag("meteor");
                float speed = asteroids[0].GetComponent<Rigidbody>().velocity.magnitude;
                TimeToHit = (Vector3.Distance(transform.position, asteroids[0].transform.position)) / speed;
            }
            sbit2.text = "�� ����� " + kolvo + " " + "����������";
            time2.text = "����� �� ������������ " + Mathf.Round(TimeToHit-4f) + " �";

        }

    }

}
