using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Asteroid : MonoBehaviour
{
    public GameObject meteor;
    private UIAsteroid uias;
    public bool AtackFor2range = false;

    private DanjetrZone dzone;
    private GameObject dzoneobj;
    private GameObject cheatobj;

    void Start()
    {
        uias = GameObject.FindGameObjectWithTag("UIAstro").GetComponent<UIAsteroid>();
        dzone = GameObject.FindGameObjectWithTag("danger").GetComponent<DanjetrZone>();
        dzoneobj = GameObject.FindGameObjectWithTag("danger");
        cheatobj = GameObject.FindGameObjectWithTag("Cheat");

    }

    public void Begin()
    {
        StartCoroutine(WaitAndPrint(5f));
    }

    void Update()
    {
        if (uias.kolvo==3 && !uias.goto2scene)
        {
            AtackFor2range = true;
            GameObject[] ostast = GameObject.FindGameObjectsWithTag("meteor");
            for (int i=0;i< ostast.Length;i++)
            {
                Destroy(ostast[i]);
            }
            uias.kolvo = 0;
            dzone.kolvo = 0;
            dzoneobj.transform.position = new Vector3(-35.74f,11.74f,-0.59f);
            cheatobj.transform.position = new Vector3(-8.48f, 11.75f, -0.53f);
            uias.goto2scene = true;
            uias.isyes = false;
        }
        else if (uias.kolvo == 3 && uias.goto2scene)
        {
            SetResult("score", cheatobj.GetComponent<CheatController>().o_kovlo);
            SceneManager.LoadScene("MainMenu");
        }
    }

    public void SetResult(string KeyName, int Value)
    {
        PlayerPrefs.SetInt(KeyName, Value);
    }

    private IEnumerator WaitAndPrint(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        if (!AtackFor2range)
        {
            Vector3 spawnposy = new Vector3(Random.Range(70, 90), Random.Range(10, 13), Random.Range(-5f, 4.5f));
            Instantiate(meteor, spawnposy, Quaternion.identity);
            uias.isyes = true;
            StartCoroutine(WaitAndPrint(5f));
        }
        else
        {
            Vector3 spawnposy = new Vector3(Random.Range(-70, -90), Random.Range(10, 13), Random.Range(-5f, 4.5f));
            Instantiate(meteor, spawnposy, Quaternion.identity);
            uias.isyes = true;
            StartCoroutine(WaitAndPrint(5f));
        }
    }
}
