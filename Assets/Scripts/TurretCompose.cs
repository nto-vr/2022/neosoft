using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretCompose : MonoBehaviour
{
    public ThirdPart ThrowablePart;
    public RotationPart RotatablePart;
    public PlaceableObject PlaceablePart;

    public Asteroid ast;

    public GameObject RealTurret;

    void Update()
    {
        if (ThrowablePart.IsDone && RotatablePart.IsDone && PlaceablePart.IsDone)
        {
            ast.Begin();
            RealTurret.SetActive(true);
            Destroy(this.gameObject);
        }
    }
}
