using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class PlayerMove : MonoBehaviour
{
    public SteamVR_Action_Vector2 moveValue;
    public float maxspeed;
    public float sensity;
    public Rigidbody head;

    private float speed = 0.0f;

    private void FixedUpdate()
    {
        if (moveValue.axis.y>0)
        {
            Vector3 direction = Player.instance.hmdTransform.TransformDirection(new Vector3(0, 0, moveValue.axis.y));
            speed = moveValue.axis.y * sensity;
            speed = Mathf.Clamp(speed,0,maxspeed);
            transform.position += speed * Time.deltaTime * Vector3.ProjectOnPlane(direction, Vector3.up);
        }

    }
}
